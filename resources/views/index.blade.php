@extends("layouts.main")

@section("content")
    <div class="row" style="margin:0; padding:0; width:100%;">
        <div class="col-sm-12 d-sm-none d-lg-block col-lg-2 sidebar">
            @include("partials.navigation")
        </div>
        <div class="col-sm-12 col-md-12 col-lg-10 mainContent" data-spy="scroll" data-target="#nav">

            @include("partials.sections.introduction")

            {{--Objects--}}
            @include("partials.sections.objects.layout")

            @include("partials.sections.objects.hex")

            {{--Resources--}}
            @include("partials.sections.resources.generate")

            @include("partials.sections.resources.layouts")

            {{--Top3--}}
            @include("partials.sections.resources.tophexes")

        </div>
    </div>
@endsection

