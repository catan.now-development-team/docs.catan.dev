<html>
    <head>
        <title>{{env("APP_NAME")}}</title>

        <link rel="stylesheet" href="{{asset("css/bootstrap.min.css")}}"/>
        {{--<link rel="stylesheet" href="{{asset("css/bootstrap-grid.min.css")}}"/>--}}

        <link rel="stylesheet" href="{{asset("css/app.css")}}?v=0"/>
    </head>

    <body>
        @yield("content")

        <script src="{{asset("js/jquery-3.3.1.js")}}"></script>
        <script src="{{asset("js/bootstrap.bundle.min.js")}}"></script>
        @stack("scripts")

        <script>
        </script>

    </body>
</html>
