<div class="logo">Catan.now <span class="highlight-bold">API</span></div>

<nav id="nav" class="navbar items">

    <nav class="nav nav-pills flex-column">
        <a class="nav-link item" href="#home">Introduction</a>

        <div class="itemgroup">objects</div>
        <a class="nav-link item" href="#layout">Layout</a>
        <a class="nav-link item" href="#hex">Hex</a>




        <div class="itemgroup">core resources</div>
        <a class="nav-link item" href="#generate">Generate</a>
        <a class="nav-link item" href="#layouts">Layouts</a>
        <nav class="nav nav-pills flex-column">
            <a class="nav-link my-1 item" href="#layouts-alllayouts"><span class="indent"></span>Retrieve all layouts</a>
            <a class="nav-link my-1 item" href="#layouts-onelayout"><span class="indent"></span>Retrieve a layout</a>
            <a class="nav-link my-1 item" href="#layouts-storelayout"><span class="indent"></span>Storing a layout</a>
        </nav>
        <a class="nav-link item" href="#besthexes">Best Intersections</a>

    </nav>
</nav>

@push("scripts")
    <script>
        $(".item").on("click", function(e){
            e.preventDefault();
            var sectionhref = $(this).attr("href");
            var section = sectionhref.substring(1, sectionhref.length);
            document.getElementById(section).scrollIntoView({behavior: "smooth"});



        });
    </script>
@endpush