<div class="api-section" id="home">
    <div class="row">
        <div class="col-lg-6">
            <div class="title">Catan.now API Reference</div>
            <div class="content">
                The Catan.Now API provides an intuitive REST interface to interact with the catan.now backend.
            </div>
        </div>
        <div class="col-lg-6">
            <br/><br/>
            <div class="endpointsblock">
                <div class="title">Base URL</div>
                <div class="endpoint">
                    <div class="route"><span class="highlight-green highlight-bold">https://</span>{{env("BASE_URL")}}</div>
                </div>
            </div>
        </div>
    </div>
</div>