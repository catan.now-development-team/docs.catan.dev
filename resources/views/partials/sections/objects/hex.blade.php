<div class="api-section" id="hex">
    <div class="row">
        <div class="col-lg-6">
            <div class="title">Hex</div>
            <div class="content">
                The Hex object represents a single hex in a layout.
                When interacting with the api. Returned hexes will always return in the format displayed on the right. When saving a layout, hexes should always have the same format or the request will be rejected.
            </div>
        </div>
        <div class="col-lg-6">
            <br/><br/>
            <div class="codeblock">
                <div class="title">Example Hex</div>
                <pre id="exampleHex"></pre>
            </div>
        </div>
    </div>
</div>

@push("scripts")
    <script>
        $.get("exampleHex").then((result) => {
            $("#exampleHex").html(JSON.stringify(result, undefined, 2));
        });


    </script>
@endpush