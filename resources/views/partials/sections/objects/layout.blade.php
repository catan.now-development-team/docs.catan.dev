<div class="api-section" id="layout">
    <div class="row">
        <div class="col-lg-6">
            <div class="title">Layout</div>
            <div class="content">
                The Layout object represents a single generated layout of the catan board.
                It consists of an array of hexes.
                When interacting with the api. Returned layouts will always return in the format displayed on the right.
            </div>
        </div>
        <div class="col-lg-6">
            <br/><br/>
            <div class="codeblock">
                <div class="title">Example Layout</div>
                <pre id="exampleLayoutObject"></pre>
            </div>
        </div>
    </div>
</div>

@push("scripts")
    <script>
        $.get("exampleLayout").then((result) => {
            $("#exampleLayoutObject").html(JSON.stringify(result, undefined, 2));
        });
    </script>
@endpush