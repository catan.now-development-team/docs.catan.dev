<div class="api-section" id="generate">
    <div class="row">
        <div class="col-lg-6">
            <div class="title">Generate</div>
            <div class="content">
                The Generate Endpoint is used to generate a new catan map. It returns a grid with the containing hexes. The Generate Endpoint requires the following parameters to be passed in:
                <table class="requiredments-table">
                    <thead>
                    <tr>
                        <th>Parameter</th>
                        <th>Usage</th>
                    </tr>
                    </thead>

                    <tbody>
                    <tr>
                        <td>width<span class="required">required</span></td>
                        <td>Defines the with of the map</td>
                    </tr>
                    <tr>
                        <td>height<span class="required">required</span></td>
                        <td>Defines the height of the map</td>
                    </tr>
                    <tr>
                        <td>random<span class="required">required</span></td>
                        <td>Defines if numbered fiches on the board are placed randomly</td>
                    </tr>
                    </tbody>


                </table>
            </div>
        </div>
        <div class="col-lg-6">
            <br/><br/>
            <div class="endpointsblock">
                <div class="title">Generate</div>
                <div class="endpoint">
                    <div class="method highlight-blue">get</div>
                    <div class="route">/api/generate</div>
                </div>
            </div>
            <br/>
            <div class="codeblock">
                <div class="title">Example Response</div>
                <pre id="exampleGenerate"></pre>
            </div>
        </div>
    </div>
</div>

@push("scripts")
    <script>
        $.get("exampleGenerate").then((result) => {
            $("#exampleGenerate").html(JSON.stringify(result, undefined, 2));
        });
    </script>
@endpush