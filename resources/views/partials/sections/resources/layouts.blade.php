<div class="api-section" id="layouts">
    <div class="row">
        <div class="col-lg-6">
            <div class="title">Layouts</div>
            <div class="content">
                Layouts represent the saved entity of a catan board. They can be saved into and retreived from the database by our api.
                Via the layouts api you can either get all the layouts or get a layout by a specific hash.

            </div>
        </div>
        <div class="col-lg-6">
            <br/><br/>
            <div class="endpointsblock">
                <div class="title">Layouts</div>
                <div class="endpoint">
                    <div class="method highlight-blue">get</div>
                    <div class="route">/api/layouts</div>
                </div>
                <div class="endpoint">
                    <div class="method highlight-blue">get</div>
                    <div class="route">/api/layouts/:hash</div>
                </div>
                <div class="endpoint">
                    <div class="method highlight-green">post</div>
                    <div class="route">/api/layouts/save</div>
                </div>
            </div>
        </div>
    </div>
    <br/><br/>
    <div class="row" id="layouts-alllayouts">
        <div class="col-lg-6">
            <div class="subtitle">Retrieving all Layouts</div>
            <div class="content">
                In order to get a list of all the layouts stored in the catan.now database you can query the /layouts/all endpoint. This gives you a list of layouts ordered by creation date.
            </div>
        </div>
        <div class="col-lg-6">
            <br/><br/>
            <div class="codeblock">
                <div class="title">
                    <div class="endpoint">
                        <div class="method highlight-blue">GET</div>
                        <div class="route">/api/layouts</div>
                    </div>
                </div>
                <div class="code">
                    <div class="numbers">
                        <div>1</div>
                    </div>
                    <div class="lines">
                        <div>curl  <span class="highlight-green">https://</span>{{env("BASE_URL")}}/api/layouts</div>
                    </div>
                </div>
            </div>
            <br/>
            <div class="codeblock">
                <div class="title">Example Response</div>
                <pre id="exampleAllLayouts"></pre>
            </div>
        </div>
    </div>
    <br/><br/><br/>
    <div class="row" id="layouts-onelayout">
        <div class="col-lg-6">
            <div class="subtitle">Retrieving a single Layout</div>
            <div class="content">
                In order to get a single layout out of all the layouts stored in the catan.now database you can query the /layouts endpoint. This gives you back a stored layout of hexagons. The only parameter here is the hash parameter which is the base64 encoded string of the layout id.
                <table class="requiredments-table">
                    <thead>
                    <tr>
                        <th>Parameter</th>
                        <th>Usage</th>
                    </tr>
                    </thead>

                    <tbody>
                    <tr>
                        <td>hash<span class="required">required</span></td>
                        <td>The base64 encoded string of a layout id. Gets returned when saving a layout to the database.</td>
                    </tr>
                    </tbody>


                </table>
            </div>
        </div>
        <div class="col-lg-6">
            <br/><br/>
            <div class="codeblock">
                <div class="title">
                    <div class="endpoint">
                        <div class="method highlight-blue">GET</div>
                        <div class="route">/api/layouts/:hash</div>
                    </div>
                </div>
                <div class="code">
                    <div class="numbers">
                        <div>1</div>
                    </div>
                    <div class="lines">
                        <div>curl  <span class="highlight-green">https://</span>{{env("BASE_URL")}}/api/layouts/NA==</div>
                    </div>
                </div>
            </div>
            <br/>
            <div class="codeblock">
                <div class="title">Example Response</div>
                <pre id="exampleLayout"></pre>
            </div>
        </div>
    </div>
    <br/><br/><br/>
    <div class="row" id="layouts-storelayout">
        <div class="col-lg-6">
            <div class="subtitle">Storing a Layout</div>
            <div class="content">
                In order to store a layout...
                <table class="requiredments-table">
                    <thead>
                    <tr>
                        <th>Parameter</th>
                        <th>Usage</th>
                    </tr>
                    </thead>

                    <tbody>
                    <tr>
                        <td>hexes(array of hexes)<span class="required">required</span></td>
                        <td>The array of hex objects to store in the database</td>
                    </tr>
                    </tbody>


                </table>
            </div>
        </div>
        <div class="col-lg-6">
            <br/><br/>
            <div class="codeblock">
                <div class="title">
                    <div class="endpoint">
                        <div class="method highlight-green">POST</div>
                        <div class="route">/api/layouts/save</div>
                    </div>
                </div>
                <div class="code">
                    <div class="numbers">
                        <div>1</div>
                    </div>
                    <div class="lines">
                        <div>curl  <span class="highlight-green">https://</span>{{env("BASE_URL")}}/api/layouts/save</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push("scripts")
    <script>
        $.get("exampleAllLayouts").then((result) => {
            $("#exampleAllLayouts").html(JSON.stringify(result, undefined, 2));
        });

        $.get("exampleSingleLayout").then((result) => {
            $("#exampleLayout").html(JSON.stringify(result, undefined, 2));
        });

    </script>
@endpush