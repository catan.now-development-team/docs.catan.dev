<div class="api-section" id="besthexes">
    <div class="row">
        <div class="col-lg-6">
            <div class="title">Best Hex Intersections</div>
            <div class="content">
                To get a summary of the best intersections between the hexes you can query the top intersections endpoint. This will return a list of x and y coordinates with a top/bottom indication of where a top intersection sits..
                <table class="requiredments-table">
                    <thead>
                    <tr>
                        <th>Parameter</th>
                        <th>Usage</th>
                    </tr>
                    </thead>

                    <tbody>
                    <tr>
                        <td>hexes (list of hex objects)<span class="required">required</span></td>
                        <td>The </td>
                    </tr>
                    </tbody>


                </table>
            </div>
        </div>
        <div class="col-lg-6">
            <br/><br/>
            <div class="endpointsblock">
                <div class="title">TopIntersections</div>
                <div class="endpoint">
                    <div class="method highlight-blue">get</div>
                    <div class="route">/api/topIntersections</div>
                </div>
            </div>
            <br/>
            <div class="codeblock">
                <div class="title">Example Response</div>
                <pre id="exampleGenerate"></pre>
            </div>
        </div>
    </div>
</div>

@push("scripts")

@endpush