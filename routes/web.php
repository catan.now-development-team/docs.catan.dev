<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Hex;

Route::get('/', function () {
    return view('index');
});

Route::get('/exampleLayout', function(){
    $hex = new Hex();
    $hex->id = 179;
    $hex->layoutid = 5;
    $hex->cardtype = 0;
    $hex->porttype = 0;
    $hex->x = 5;
    $hex->y = 2;
    $hex->rollpoint = 3;
    $hex->stars = 3;
    $hex->northscore = 7;
    $hex->southscore = 3;

    $hex2 = new Hex();
    $hex2->id = 180;
    $hex2->layoutid = 5;
    $hex2->cardtype = 2;
    $hex2->porttype = 0;
    $hex2->x = 5;
    $hex2->y = 3;
    $hex2->rollpoint = 5;
    $hex2->stars = 5;
    $hex2->northscore = 3;
    $hex2->southscore = 5;

    return response()->json([$hex, $hex2]);
});

Route::get('/exampleHex', function(){
    $hex = new Hex();
    $hex->id = 179;
    $hex->layoutid = 5;
    $hex->cardtype = 0;
    $hex->porttype = 0;
    $hex->x = 5;
    $hex->y = 2;
    $hex->rollpoint = 3;
    $hex->stars = 3;
    $hex->northscore = 7;
    $hex->southscore = 3;
    return response()->json($hex);
});

Route::get('/exampleGenerate', function(){
   return response()->json(["success" => "true", "grid" => []]);
});

Route::get('/exampleAllLayouts', function(){
    return response()->json(["success" => "true", "layouts" => []]);
});

Route::get('/exampleSingleLayout', function(){
    $hex = new Hex();
    $hex->id = 179;
    $hex->layoutid = 5;
    $hex->cardtype = 0;
    $hex->porttype = 0;
    $hex->x = 5;
    $hex->y = 2;
    $hex->rollpoint = 3;
    $hex->stars = 3;
    $hex->northscore = 7;
    $hex->southscore = 3;

    $hex2 = new Hex();
    $hex2->id = 180;
    $hex2->layoutid = 5;
    $hex2->cardtype = 2;
    $hex2->porttype = 0;
    $hex2->x = 5;
    $hex2->y = 3;
    $hex2->rollpoint = 5;
    $hex2->stars = 5;
    $hex2->northscore = 3;
    $hex2->southscore = 5;
    return response()->json(["success" => "true", "grid" => [$hex, $hex2]]);
});